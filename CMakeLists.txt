# Release build type:
# cmake -D CMAKE_BUILD_TYPE=Release
#
# Debug build type (by default):
# cmake -D CMAKE_BUILD_TYPE=Debug
#
# For verbose makefile
# cmake -D CMAKE_VERBOSE_MAKEFILE=ON
#


cmake_minimum_required(VERSION 2.6)

project(video-gazer)


set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake")


# Build type by default is Debug
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Debug)
endif(NOT CMAKE_BUILD_TYPE)


find_package(OpenCV REQUIRED)


message(STATUS "Build type: " ${CMAKE_BUILD_TYPE})
message(STATUS "OpenCV version: ${OpenCV_VERSION_MAJOR}.${OpenCV_VERSION_MINOR}.${OpenCV_VERSION_PATCH}")


AUX_SOURCE_DIRECTORY("src" SRC_LIST)


include_directories(${OpenCV_INCLUDE_DIR})


add_executable(${PROJECT_NAME} ${SRC_LIST})


target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS})
