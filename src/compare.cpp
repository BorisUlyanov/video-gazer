///////////////////////////////////////////////////////////////////////////////
// @copyright Copyright (c) 2015 Boris Ulyanov <boris-ulyanov@mail.ru>
//
// @license The MIT License (MIT)
//
// @brief Compare two frames
///////////////////////////////////////////////////////////////////////////////

#include "compare.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <numeric>
#include <stdio.h>

// declared in main.cpp
extern bool verbose;

static double treshhold_value = 20.0;
static double min_area_size = 500.0;
static bool outline = false;

static std::vector<int> zones;
static std::vector<std::vector<cv::Point> > contours;

static double area_sum;


void compare_init(int threshold, int min_area, bool _outline) {
    if (threshold < 1)
        fprintf(stderr, "Error: threshold value too small (get %d)\n", threshold);
    if (min_area < 0)
        fprintf(stderr, "Error: min_area must be >= 0 (get %d)\n", min_area);

    treshhold_value = threshold;
    min_area_size = min_area;
    outline = _outline;
}


static void diff(const cv::Mat &frame_cur, const cv::Mat &frame_pre) {
    // Blur images
    cv::Mat a_blur, b_blur;
    cv::blur(frame_cur, a_blur, cv::Size(4, 4));
    cv::blur(frame_pre, b_blur, cv::Size(4, 4));

    // Get absolute difference
    cv::Mat c;
    cv::absdiff(b_blur, a_blur, c);

    std::vector<cv::Mat> channels;
    cv::split(c, channels);

    cv::Mat d = cv::Mat::zeros(c.size(), CV_8UC1);
    for (int i = 0; i < channels.size(); i++) {
        cv::Mat thresh_fr;
        cv::threshold(channels[i], thresh_fr, treshhold_value, 255, CV_THRESH_BINARY);
        d |= thresh_fr;
    }

    cv::Mat kernel, e;
    cv::getStructuringElement(cv::MORPH_RECT, cv::Size(10, 10));
    cv::morphologyEx(d, e, cv::MORPH_CLOSE, kernel, cv::Point(-1, -1));

    zones.clear();
    contours.clear();
    cv::findContours(e.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

    area_sum = 0;
    for (int i = 0; i < contours.size(); i++) {
        // Calculate area
        double area = cv::contourArea(contours[i]);
        // if (area < min_area_size)
        //     continue;
        area_sum += area;
        zones.push_back(i);
    }
}


double compare_diff_val(const cv::Mat &frame_cur, const cv::Mat &frame_pre) {
    diff(frame_cur, frame_pre);
    return (area_sum);
}


double compare_diff_snapshot(const cv::Mat &frame_cur, const cv::Mat &frame_pre,
                             const char* filename) {
    diff(frame_cur, frame_pre);
    if (area_sum < min_area_size) return (area_sum);

    cv::Mat frame_res;

    if (outline)
        frame_res = frame_cur.clone();
    else
        frame_res = frame_cur;

    if (outline)
        for (int i = 0; i < zones.size(); ++i)
            cv::drawContours(frame_res, contours, zones[i], CV_RGB(255, 255, 0));

    bool rc = cv::imwrite(filename, frame_res);
    if (!rc)
        fprintf(stderr, "Error: can't write image [%s]\n", filename);

    return (area_sum);
}


void compare_fini() {
    // empty
}
