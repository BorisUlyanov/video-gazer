///////////////////////////////////////////////////////////////////////////////
// @copyright Copyright (c) 2015 Boris Ulyanov <boris-ulyanov@mail.ru>
//
// @license The MIT License (MIT)
//
// @brief Compare two frames
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <opencv2/core/core.hpp>


void compare_init(int threshold, int min_area, bool outline);

double compare_diff_val(const cv::Mat &frame_cur, const cv::Mat &frame_pre);

double compare_diff_snapshot(const cv::Mat &frame_cur, const cv::Mat &frame_pre,
                             const char* filename);

void compare_fini();
