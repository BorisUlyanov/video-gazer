///////////////////////////////////////////////////////////////////////////////
// @copyright Copyright (c) 2015 Boris Ulyanov <boris-ulyanov@mail.ru>
//
// @license The MIT License (MIT)
//
// @brief Vork with video file
///////////////////////////////////////////////////////////////////////////////

#include "video.h"

#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>

// declared in main.cpp
extern bool verbose;

static cv::VideoCapture capture;
static size_t frames_count = 0;
static size_t next_pos = 0;
static size_t pos_step = 0;
static size_t fps = 0;


// return true if successful
bool video_init(const char *video_file, int init_skip, int step) {

    capture.open(video_file);
    if (!capture.isOpened()) {
        fprintf(stderr, "Error: can\'t open video: %s\n", video_file);
        return (false);
    }

    fps = capture.get(CV_CAP_PROP_FPS);
    int w = capture.get(CV_CAP_PROP_FRAME_WIDTH);
    int h = capture.get(CV_CAP_PROP_FRAME_HEIGHT);
    frames_count = capture.get(CV_CAP_PROP_FRAME_COUNT);

    float l = (float)frames_count / fps;
    if (verbose)
        fprintf(stderr, "Open video: %s %dx%d@%ld, len = %.0fs\n", video_file, w, h, fps, l);

    next_pos = init_skip / 1000. * fps;
    pos_step = step / 1000. * fps;

    return (true);
}


// return true if successful (false = EOF)
bool video_get_next(cv::Mat* p_frame, uint32_t *timestamp) {
    if (next_pos >= frames_count)
        return (false);

    capture.set(CV_CAP_PROP_POS_FRAMES, next_pos);
    if (!capture.read(*p_frame)){
        fprintf(stderr, "Error: unexpected error read at frame position = %ld\n", next_pos);
        return (false);
    }

    *timestamp = next_pos * 1000 / fps;
    next_pos += pos_step;
    return (true);
}


void video_fini() {
    capture.release();
}
