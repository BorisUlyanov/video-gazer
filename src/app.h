///////////////////////////////////////////////////////////////////////////////
// @copyright Copyright (c) 2015 Boris Ulyanov <boris-ulyanov@mail.ru>
//
// @license The MIT License (MIT)
//
// @brief Main app logic
///////////////////////////////////////////////////////////////////////////////

#pragma once


// return true if successful
bool app_init(const char *config_file,
              const char *output_dir,
              const char *video_file);

// return true if successful
bool app_run();

void app_fini();
