///////////////////////////////////////////////////////////////////////////////
// @copyright Copyright (c) 2015 Boris Ulyanov <boris-ulyanov@mail.ru>
//
// @license The MIT License (MIT)
//
// @brief entry point
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "app.h"


// global flag
bool verbose = false;


static char about_str[] = \
"\n"
"  Usage: ./video-gazer [options] video_file\n"
"\n"
"  Options:\n"
"\n"
"    -V, --version                 output program version\n"
"    -v, --verbose                 verbose output\n"
"    -h, --help                    output help information\n"
"    -c, --config <config_file>    set configuration file (default - ../etc/video-gazer.yaml)\n"
"    -d, --dir <dest_dir>          output snapshot directory (if not defined - only text report generated)\n";


//////////
// main //
//////////
int main(int argc, const char *argv[]) {
    const char *config = "../etc/video-gazer.yaml";
    const char *video = NULL;
    const char *dir = NULL;

    // parse command line
    for (int arg_idx = 1; arg_idx < argc; arg_idx++) {
        const char *cur_arg = argv[arg_idx];
        const char *next_arg = NULL;
        if ((arg_idx + 1) < argc)
            next_arg = argv[arg_idx + 1];

        if (!strcmp(cur_arg, "-V") || !strcmp(cur_arg, "--version")) {
            printf("%s 0.1\n", argv[0]);
            exit(EXIT_SUCCESS);
        }
        if (!strcmp(cur_arg, "-v") || !strcmp(cur_arg, "--verbose")) {
            verbose = true;
            continue;
        }
        if (!strcmp(cur_arg, "-h") || !strcmp(cur_arg, "--help")) {
            printf("%s\n", about_str);
            exit(EXIT_SUCCESS);
        }
        if (!strcmp(cur_arg, "-c") || !strcmp(cur_arg, "--config")) {
            if (next_arg) {
                config = next_arg;
                arg_idx++;
                continue;
            } else {
                fprintf(stderr, "Error: config argument must contain argument\n");
                exit(EXIT_FAILURE);
            }
        }
        if (!strcmp(cur_arg, "-d") || !strcmp(cur_arg, "--dir")) {
            if (next_arg) {
                dir = next_arg;
                arg_idx++;
                continue;
            } else {
                fprintf(stderr, "Error: dir argument must contain argument\n");
                exit(EXIT_FAILURE);
            }
        }
        if (arg_idx == (argc - 1)) {
            video = cur_arg;
            continue;
        } else {
            fprintf(stderr, "Error: unexpected argument - [%s]\n", cur_arg);
            exit(EXIT_FAILURE);
        }
    }

    if (video == NULL) {
        fprintf(stderr, "Error: video file undefined\n");
        exit(EXIT_FAILURE);
    }

    // run app
    if (!app_init(config, dir, video))
        exit(EXIT_FAILURE);

    if (!app_run())
        exit(EXIT_FAILURE);

    app_fini();

    return (0);
}
