///////////////////////////////////////////////////////////////////////////////
// @copyright Copyright (c) 2015 Boris Ulyanov <boris-ulyanov@mail.ru>
//
// @license The MIT License (MIT)
//
// @brief Main app logic
///////////////////////////////////////////////////////////////////////////////

#include "app.h"
#include "video.h"
#include "compare.h"

#include <opencv2/core/core.hpp>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>

#define BUFF_SIZE 200

// declared in main.cpp
extern bool verbose;

static bool text_only = true;
static char* output_dir = NULL;
static int min_area = 0;

static const char* timestamp2filename(int hh, int mm, float ss_s) {
    static char buff[BUFF_SIZE];
    assert(output_dir);
    snprintf(buff, BUFF_SIZE, "%s%02d_%02d_%04.1f.jpg", output_dir, hh, mm, ss_s);
    return (buff);
}


static int create_dir(const char *dir_name) {
    static char buff[BUFF_SIZE];
    assert(dir_name);
    size_t l = strlen(dir_name);
    assert(l > 0);
    assert((l - 2) < BUFF_SIZE);

    int status = mkdir(dir_name, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (status != 0)
        if (errno != EEXIST) {
            perror("Create output dir");
            return (-1);
        } else if (verbose)
            fprintf(stderr, "Output dir exists\n");

    strncpy(buff, dir_name, sizeof(buff));
    if (buff[l - 1] != '/') {
        buff[l] = '/';
        buff[l + 1] = '\0';
    }

    output_dir = buff;
    return (0);
}


bool app_init(const char *config_file,
              const char *_output_dir, const char *video_file) {
    assert(config_file);
    assert(video_file);

    if (verbose) {
        fprintf(stderr, "Initialize app with params:\n");
        fprintf(stderr, "\t- config_file: [%s]\n", config_file);
        fprintf(stderr, "\t- video_file:  [%s]\n", video_file);
        fprintf(stderr, "\t- output_dir:  [%s]\n", _output_dir?_output_dir:"not defined");
    }

    if (_output_dir) {
        if (verbose)
            fprintf(stderr, "Try create output dir - [%s]\n", _output_dir);
        text_only = false;
        if (create_dir(_output_dir)) {
            fprintf(stderr, "Can't create output dir - [%s]\n", _output_dir);
            return (false);
        }
    }

    if (verbose)
        fprintf(stderr, "Start read config file - %s\n", config_file);
    cv::FileStorage fs(config_file, cv::FileStorage::READ);
    if (!fs.isOpened()) {
        fprintf(stderr, "Can't load config file - [%s]\n", config_file);
        return (false);
    }

    int step, init_skip;
    int threshold;
    bool outline;

    fs["step"] >> step;
    fs["init-skip"] >> init_skip;
    fs["threshold"] >> threshold;
    fs["min-area"] >> min_area;
    fs["outline-diff"] >> outline;
    fs.release();

    if (verbose) {
        fprintf(stderr, "Readed configuration:\n");
        fprintf(stderr, "\t- step:         %d \n", step);
        fprintf(stderr, "\t- init-skip:    %d \n", init_skip);
        fprintf(stderr, "\t- threshold:    %d \n", threshold);
        fprintf(stderr, "\t- min-area:     %d \n", min_area);
        fprintf(stderr, "\t- outline-diff: %d \n", outline);
    }

    if (!video_init(video_file, init_skip, step))
        return (false);

    compare_init(threshold, min_area, outline);

    return (true);
}


bool app_run() {
    bool rc;
    uint32_t timestamp;

    cv::Mat frame1, frame2;
    cv::Mat* cur_frame = &frame1;
    cv::Mat* pre_frame = &frame2;

    rc = video_get_next(pre_frame, &timestamp);
    if (!rc) {
        fprintf(stderr, "Error: read initial frame\n");
        return (false);
    }

    while (true) {
        rc = video_get_next(cur_frame, &timestamp);
        if (!rc)
            break;

        int hh = timestamp / (3600 * 1000);
        timestamp -= hh * 3600 * 1000;
        int mm = timestamp / (60 * 1000);
        timestamp -= mm * 60 * 1000;
        float ss_s = timestamp / 1000.;

        double diff;
        if (text_only)
            diff = compare_diff_val(*cur_frame, *pre_frame);
        else  {
            const char *fn = timestamp2filename(hh, mm, ss_s);
            diff = compare_diff_snapshot(*cur_frame, *pre_frame, fn);
        }

        if ((diff > min_area) || verbose)
            printf("%02d:%02d:%04.1f  -  %6.0f\n", hh, mm, ss_s, diff);

        cv::Mat *tmp = pre_frame;
        pre_frame = cur_frame;
        cur_frame = tmp;
    }

    return (true);
}


void app_fini() {
    video_fini();
    compare_fini();
}
