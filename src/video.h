///////////////////////////////////////////////////////////////////////////////
// @copyright Copyright (c) 2015 Boris Ulyanov <boris-ulyanov@mail.ru>
//
// @license The MIT License (MIT)
//
// @brief Vork with video file
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <opencv2/core/core.hpp>


// init_skip - skip time from start file (ms)
// step - step between frames (ms)
// return true if successful
bool video_init(const char *video_file, int init_skip, int step);

// fill next frame and return it timestamp
// return true if successful (false = EOF)
bool video_get_next(cv::Mat* p_frame, uint32_t *timestamp);

void video_fini();
