# video-gazer

Search dynamical moments in video file


## Dependices

[OpenCV-2.4.10](http://opencv.org/)

Install as deb package:

```sudo apt-get install libopencv-dev```

or install from source: ([HOWTO for Ubuntu](https://help.ubuntu.com/community/OpenCV))


## Build

```
mkdir build
cd build
cmake ..
make
```

## Usage

For generate only text report:
```
./video-gazer path_to_video.avi
```

For make snapshots:
```
./video-gazer -d path_to_save_snapshots path_to_video.avi
```


## License

The MIT License (MIT)
